<?php include('header.php'); ?>
<div class="content">
<div class="sidebar">
  <?php echo $sidebar; ?>
</div>
<div class="main">
  <?php echo $content; ?>
</div>
</div>
<?php include('footer.php'); ?>