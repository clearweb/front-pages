<?php namespace Clearweb\FrontPages\Contact;

use Clearweb\Clearwebapps\Form\Form;
use Clearweb\Clearwebapps\Form\TextField;
use Clearweb\Clearwebapps\Form\SubmitField;

class SocialMediaSettingsWidget extends \Clearweb\Clearwebapps\Widget\SettingWidget
{
    public function init()
    {
        $this->setName('social-media-settings');
        $this->setTitle(trans('front-pages::contact.social_media'));
        
        $this->setSettings(array(
                                 'facebook',
                                 'twitter',
                                 'linkedin',
                                 'google_plus',
                                 'instagram',
                                 ));
        
        $form = with(new Form)
            ->addField(with(new TextField)->setName('facebook')->setLabel(trans('front-pages::contact.facebook')))
            ->addField(with(new TextField)->setName('twitter')->setLabel(trans('front-pages::contact.twitter')))
            ->addField(with(new TextField)->setName('linkedin')->setLabel(trans('front-pages::contact.linkedin')))
            ->addField(with(new TextField)->setName('google_plus')->setLabel(trans('front-pages::contact.google_plus')))
            ->addField(with(new TextField)->setName('instagram')->setLabel(trans('front-pages::contact.instagram')))
            ->addField(
                       with(new SubmitField)
                       ->setName('submit')
                       ->setLabel(trans('clearwebapps::form_widget.submit'))
                       )
            ;
        
        $this->setForm($form);
        
        
        parent::init();
        
        $this->getValidator()->setRules(array());
        
        return $this;
    }
}