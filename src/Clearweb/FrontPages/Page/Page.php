<?php namespace Clearweb\FrontPages\Page;

use Clearweb\FrontPages\Layout\OneColumnLayout;

use Clearweb\FrontPages\Widget\TopCitiesWidget;

use Clearweb\FrontPages\NoTitleException;

class Page extends \Clearweb\Clearworks\Page\LinearPage
{
    protected $title   = '';
    protected $description = '';
    protected $heading = '';
    protected $showSearchForm = false;
    
    public function init()
    {
        $this->loadLayout();
        return parent::init();
    }
    
    public function execute()
    {
        if ($this->getTitle() == '') {
            throw new NoTitleException("Page with slug '{$this->getSlug()}' has no title");
        }
        
        $this->getLayout()->setTitle($this->getTitle());
        $this->getLayout()->setDescription($this->getDescription());
        return parent::execute();
    }
    
    
    public function getTitle()
    {
        return $this->title;
    }
    
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }
    
    public function getDescription()
    {
        return $this->description;
    }
    
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }
    
    public function getHeading()
    {
        return $this->heading;
    }
    
    public function setHeading($heading)
    {
        $this->heading = $heading;
        return $this;
    }
    
    
    protected function loadLayout()
    {
        $this->setLayout(new OneColumnLayout);
    }
}