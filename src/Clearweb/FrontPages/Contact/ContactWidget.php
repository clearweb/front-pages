<?php namespace Clearweb\FrontPages\Contact;

use Clearweb\Clearworks\Widget\ContainerWidget;
use Clearweb\Clearworks\Content\RawHTML;

use View;
use Settings;
use FileManager;

class ContactWidget extends ContainerWidget
{
    public function init()
    {
        parent::init();
        
        $logo = FileManager::getFile(Settings::get('logo'));
        $logoUrl = $logo->getUrl();
        
        
        $this->getContainer()
            ->addClass('contact-widget')
            ->addViewable(
                          with(new RawHTML())
                          ->setHTML(
                                    View::make('front-pages::contact_widget')
                                    ->with('logo', $logoUrl)
                                    ->with('companyName', Settings::get('company_name'))
                                    ->with('street', Settings::get('street'))
                                    ->with('houseNumber', Settings::get('house_number'))
                                    ->with('postalCode', Settings::get('postal_code'))
                                    ->with('city', Settings::get('city'))
                                    ->with('country', Settings::get('country'))
                                    ->with('phone', Settings::get('phone'))
                                    ->with('email', Settings::get('email'))
                                    )
                          )
            ;
        
        return $this;
    }
}

