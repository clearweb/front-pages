<?php namespace Clearweb\FrontPages\Slider\Widget;

use Clearweb\Clearworks\Widget\ContainerWidget;
use Clearweb\FrontPages\Slider\Content\Slider;
use Clearweb\FrontPages\Slider\Slide;

class SliderWidget extends ContainerWidget
{
    private $slider = null;
    
    public function getSlider()
    {
        return $this->slider;
    }
    
    public function setSlider(Slider $slider)
    {
        $this->slider = $slider;
        
        return $this;
    }

    public function __construct()
    {
        parent::__construct();
        $this->setSlider(new Slider);
    }
    
    public function init()
    {
        parent::init();
        
        $this->getSlider()->setSlides(Slide::all()->all());
        
        $this->getContainer()->addViewable($this->getSlider())
            ->addClass('slider')
            ;
        
        return $this;
    }
}