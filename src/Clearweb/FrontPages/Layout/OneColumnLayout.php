<?php namespace Clearweb\FrontPages\Layout;

class OneColumnLayout extends Layout
{
    protected function getViewName() {
        return 'front-pages::one_column';
    }
    
    function loadContainers()
    {
        $this->containers = array('header', 'content', 'footer');
        $this->containers_loaded = true;
    }
}