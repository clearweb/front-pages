<!DOCTYPE html>
<html>
<head>

<?php

foreach(array_unique($stylesheets) as $stylesheet) {
	 echo '<link rel="stylesheet" href="'.asset($stylesheet).'" />'.PHP_EOL;
}
?>
<?php
$reordered_js = array('http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.js');

foreach(array_unique($javascripts) as $javascript) {
	if ($javascript != 'http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.js')
		$reordered_js[] = $javascript;
}


foreach(array_unique($reordered_js) as $javascript) {
	if (preg_match('#^https?://.*#', $javascript)) {
		echo '<script src="'.$javascript.'"></script>'.PHP_EOL;
	} else {
		echo '<script src="'.asset($javascript).'"></script>'.PHP_EOL;
	}
}
?>

<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700' rel='stylesheet' type='text/css'>
<title><?php echo $title; ?></title>
<?php if ( ! empty($description)): ?>
<meta title="description" value="<?php echo $description; ?>" />
<?php endif; ?>
</head>
<body>
<header>
<div class="head">
    <?php echo $header; ?>
</div>
</header>