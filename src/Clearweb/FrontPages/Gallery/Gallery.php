<?php namespace Clearweb\FrontPages\Gallery;

use Clearweb\FrontPages\Content\Image;

use Clearweb\Clearworks\Contracts\IViewable;

class Gallery implements IViewable
{
    private $images = array();
    
    public function getImages()
    {
        return $this->images;
    }
    
    public function setImages(array $images)
    {
        $this->images = $images;
        
        return $this;
    }
    
    public function addImage(Image $image) {
        $this->images[] = $image;
        
        return $this;
    }
    
    public function getView() {
        $html = '<div class="fotorama" data-nav="thumbs">';
        foreach($this->getImages() as $image) {
            $html .= $image->getView();
        }
        $html .= '</div>';
        
        return $html;
    }
    
    public function getStyles()
    {
        return array('http://cdnjs.cloudflare.com/ajax/libs/fotorama/4.6.3/fotorama.css');
    }
    
    public function getScripts()
    {
        return array('http://cdnjs.cloudflare.com/ajax/libs/fotorama/4.6.3/fotorama.js');
    }
}