<?php namespace Clearweb\FrontPages\Content;

use Clearweb\Clearworks\Contracts\IViewable;
use Clearweb\Clearwebapps\File\File;

class Image implements IViewable
{
    private $file;
    
    public function setFile(File $file)
    {
        $this->file = $file;
        return $this;
    }
    
    public function getFile()
    {
        return $this->file;
    }
    
    public function getStyles()
    {
        return array();
    }
    
    public function getScripts()
    {
        return array();
    }
    
    public function getView()
    {
        return "<img src='{$this->getFile()->getUrl()}' />";
    }
}