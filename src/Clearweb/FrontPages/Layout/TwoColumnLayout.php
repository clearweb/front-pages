<?php namespace Clearweb\FrontPages\Layout;

class TwoColumnLayout extends Layout
{
    protected function getViewName() {
        return 'front-pages::two_column';
    }
    
    public function loadContainers()
    {
        $this->containers = array('header', 'content', 'sidebar', 'footer');
        $this->containers_loaded = true;
    }
}