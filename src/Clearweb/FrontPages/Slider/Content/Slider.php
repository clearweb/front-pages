<?php namespace Clearweb\FrontPages\Slider\Content;

use Clearweb\Clearworks\Contracts\IViewable;

use View;

class Slider implements IViewable 
{
    private $slides = array(); 
    private $viewName = 'front-pages::slider';
    
    public function getSlides()
    {
        return $this->slides;
    }
    
    public function setSlides(array $slides)
    {
        $this->slides = $slides;
        
        return $this;
    }

    public function getViewName()
    {
        return $this->viewName;
    }
    
    public function setViewName($viewName)
    {
        $this->viewName = $viewName;
        
        return $this;
    }
    
    public function getScripts()
    {
        return array(
                     '/packages/clearweb/front-pages/js/idangerous.swiper.js',
                     '/packages/clearweb/front-pages/js/slider.js',
                     );
    }
    
    public function getStyles()
    {
        return array(
                     '/packages/clearweb/front-pages/css/idangerous.swiper.css',
                     );
    }
    
    public function getView()
    {
        return View::make($this->getViewName())
            ->with('slides', $this->getSlides())
            ;
    }
}