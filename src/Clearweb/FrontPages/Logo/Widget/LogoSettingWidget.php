<?php namespace Clearweb\FrontPages\Logo\Widget;

use Clearweb\Clearwebapps\Widget\SettingWidget;

use Clearweb\Clearwebapps\Form\Form;
use Clearweb\Clearwebapps\Form\ImageUploadField;
use Clearweb\Clearwebapps\Form\SubmitField;

class LogoSettingWidget extends SettingWidget
{
    public function init()
    {
        $this->setName('logo-settings');
        $this->setTitle(trans('front-pages::logo.logo_settings'));
        
        $this->setSettings(array('logo'));
        
        $form = with(new Form)
            ->addField(
                       with(new ImageUploadField)
                       ->setName('logo')
                       ->setLabel(trans('front-pages::logo.logo'))
                       )
            ->addField(
                       with(new SubmitField)
                       ->setName('submit')
                       ->setLabel(trans('clearwebapps::form_widget.submit'))
                       )
                     ;
        
        $this->setForm($form);
        
        parent::init();
        return $this;
    }
}