<?php namespace Clearweb\FrontPages\Contact;

use Illuminate\Support\ServiceProvider;

use Event;
use Settings;

class ContactServiceProvider extends ServiceProvider
{
    /**
	 * Indicates if loading of the provider is deferred.
	 *
	 * @var bool
	 */
	protected $defer = false;
    
    /**
     * Bootstrap the application events.
	 *
	 * @return void
	 */
	public function boot()
	{
        
	}
    
    /**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
        Event::listen(
                      'clearwebapps.booted',
                      function () {
                          Settings::addPackage(new \Clearweb\FrontPages\Contact\SettingPackage);
                      }
                      );
    }
    
	/**
	 * Get the services provided by the provider.
	 *
	 * @return array
	 */
	public function provides()
	{
		return array();
	}
}