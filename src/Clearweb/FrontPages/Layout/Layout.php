<?php namespace Clearweb\FrontPages\Layout;

use Clearweb\Clearworks\Layout\LinearLayout;

use Clearweb\FrontPages\NoTitleException;

abstract class Layout extends LinearLayout
{
    protected $title       = '';
    protected $description = '';
    protected $heading     = '';
    
    /**
     * returns the name of the view for the layout
     */
    abstract protected function getViewName();
    
    public function init()
    {
        parent::init();
    }
    
    public function getTitle()
    {
        return $this->title;
    }
    
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    public function getDescription()
    {
        return $this->description;
    }
    
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }
    
    public function getHeading()
    {
        return $this->heading;
    }
    
    public function setHeading($heading)
    {
        $this->heading = $heading;
        return $this;
    }
    
    
    
    public function getStyles()
    {
        return array_merge(parent::getStyles(),
                           array('http://ajax.aspnetcdn.com/ajax/jquery.ui/1.10.4/themes/blitzer/jquery-ui.min.css')
                           );
    }

    /*
    public function getScripts()
    {
    return array_merge(parent::getScripts(), array('/packages/clearweb/front-pages/js/main.js'));
    }
    */
    
    /**
     * @throws NoTitleException
     */
    function getView()
    {
        if ($this->getTitle() == '') {
            throw new NoTitleException;
        }
        
        $vars = array(
                      'title'       => $this->getTitle(),
                      'description' => $this->getDescription(),
                      'heading'     => $this->getHeading(),
                      'stylesheets' => $this->getStyles(),
                      'javascripts' => $this->getScripts(),
                      );
        
        foreach($this->getContainers() as $container) {
            $vars[$container] = $this->getContainerView($container);
        }
        
        return \View::make(
                           $this->getViewName(),
                           $vars
                           );
    }
}