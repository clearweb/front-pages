<div class="slider-wrapper">
  <div class="swiper-container">
    <div class="swiper-wrapper">
      <?php
	 foreach($slides as $slide):
         $slideImage = FileManager::getFile($slide->image);
      ?>
      <div class="swiper-slide">
        <img src="<?php echo $slideImage->getUrl(); ?>" />
      </div>
      <?php endforeach; ?>
    </div>
  </div>
</div>
