<?php namespace Clearweb\FrontPages\Slider;

class Slide extends \Eloquent
{
    protected $fillable = array('image', 'title');
    
    function getName()
    {
        return $this->title;
    }
}