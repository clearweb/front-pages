<?php namespace Clearweb\FrontPages;

use Illuminate\Support\ServiceProvider;

use Clearworks;

class FrontPagesServiceProvider extends ServiceProvider {

	/**
	 * Indicates if loading of the provider is deferred.
	 *
	 * @var bool
	 */
	protected $defer = false;

	/**
	 * Bootstrap the application events.
	 *
	 * @return void
	 */
	public function boot()
	{
		$this->package('clearweb/front-pages');
        
	}
    
	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
        $clearworks = $this->app->make('clearworks');
		$clearworks->make('public', 'public', true);
	}

	/**
	 * Get the services provided by the provider.
	 *
	 * @return array
	 */
	public function provides()
	{
		return array();
	}

}
