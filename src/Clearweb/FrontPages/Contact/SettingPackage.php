<?php namespace Clearweb\FrontPages\Contact;

class SettingPackage extends \Clearweb\Clearwebapps\Setting\SettingPackage
{
    public function __construct()
    {
        $this->setTitle(trans('front-pages::contact.contact_settings'));
        $this->setName('contact-settings');
        
        $this->addWidget(new SettingsWidget);
        $this->addWidget(new SocialMediaSettingsWidget);
    }
}