<?php namespace Clearweb\FrontPages\Logo\Widget;

use Clearweb\Clearworks\Widget\ContainerWidget;
use Clearweb\FrontPages\Content\Image;

use FileManager;
use Settings;

class LogoWidget extends ContainerWidget
{
    public function init()
    {
        parent::init();
        
        $file = FileManager::getFile(Settings::get('logo'));
        
        $this->getContainer()->addViewable(with(new Image)->setFile($file))
            ->addClass('logo')
            ;
        
        $this->addStyle('/packages/clearweb/front-pages/css/logo.css');
        
        return $this;
    }
}