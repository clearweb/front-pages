<ul>
  <?php if ( ! empty($logo)): ?>
  <li class="logo">
    <img src="<?php echo $logo; ?>" />
  </li>
  <?php endif; ?>
  
  <?php if ( ! empty($companyName)): ?>
  <li class="company">
    <?php echo $companyName; ?>
  </li>
  <?php endif; ?>
  
  <?php if ( ! empty($street) && ! empty($houseNumber)): ?>
  <li class="address1">
    <?php echo $street.' '.$houseNumber; ?>
  </li>
  <?php endif; ?>
  
  <?php if ( ! empty($postalCode) && ! empty($city)): ?>
  <li class="city-info">
    <?php echo $postalCode.' '.$city; ?>
  </li>
  <?php endif; ?>
  
  <?php if ( ! empty($phone)): ?>
  <li class="phone">
    <?php echo $phone; ?>
  </li>
  <?php endif; ?>
  
  <?php if ( ! empty($email)): ?>
  <li class="email">
    <?php echo $email; ?>
  </li>
  <?php endif; ?>
</ul>
