<?php namespace Clearweb\FrontPages\Slider\Widget;

use FileManager;

class ListWidget extends \Clearweb\Clearwebapps\Eloquent\ListWidget
{
    function getModelClass()
    {
        return '\Clearweb\FrontPages\Slider\Slide';
    }
    
    function init()
    {
        parent::init();

        /*
         * @todo add rank change links
         */
        $this->getList()
            /*
            ->replaceColumn(
                            'image',
                            function ($row) {
                                $image = FileManager::getFile($row['image']);
                                
                                if (empty($image)) {
                                    return '';
                                } else {
                                    return "<img height='200' src='{$image->getUrl()}' />";
                                }
                            }
                            )
            */
            ->removeColumn('image')
            //->addActionLink()
            ;
        
        return $this;
    }
}