<?php

return array(
             'contact_settings' => 'Instellingen voor contact',
             'contact_data' => 'Contactgegevens',
             
             'company_name' => 'Bedrijfsnaam',
             'street' => 'Straat',
             'house_number' => 'Huisnummer',
             'postal_code' => 'Postcode',
             'city' => 'Stad',
             'country' => 'Land',
             'phone' => 'Telefoonnummer',
             'email' => 'E-mailadres',

             
             'social_media' => 'Social Media',
             'facebook' => 'Facebook',
             'twitter' => 'Twitter',
             'linkedin' => 'LinkedIn',
             'google_plus' => 'Google+',
             'instagram' => 'Instagram',
             );