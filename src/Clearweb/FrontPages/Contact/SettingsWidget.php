<?php namespace Clearweb\FrontPages\Contact;

use Clearweb\Clearwebapps\Form\Validator;

use Clearweb\Clearwebapps\Form\Form;
use Clearweb\Clearwebapps\Form\TextField;
use Clearweb\Clearwebapps\Form\SubmitField;

class SettingsWidget extends \Clearweb\Clearwebapps\Widget\SettingWidget
{
    public function init()
    {
        $this->setName('contact-settings');
        $this->setTitle(trans('front-pages::contact.contact_data'));
        
        $this->setSettings(array(
                                 'company_name',
                                 'street',
                                 'house_number',
                                 'postal_code',
                                 'city',
                                 'country',
                                 'phone',
                                 'email',
                                 ));
        
        $form = with(new Form)
            ->addField(with(new TextField)->setName('company_name')->setLabel(trans('front-pages::contact.company_name')))
            ->addField(with(new TextField)->setName('street')->setLabel(trans('front-pages::contact.street')))
            ->addField(with(new TextField)->setName('house_number')->setLabel(trans('front-pages::contact.house_number')))
            ->addField(with(new TextField)->setName('postal_code')->setLabel(trans('front-pages::contact.postal_code')))
            ->addField(with(new TextField)->setName('city')->setLabel(trans('front-pages::contact.city')))
            ->addField(with(new TextField)->setName('country')->setLabel(trans('front-pages::contact.country')))           
            ->addField(with(new TextField)->setName('phone')->setLabel(trans('front-pages::contact.phone')))
            ->addField(with(new TextField)->setName('email')->setLabel(trans('front-pages::contact.email')))
            ->addField(
                       with(new SubmitField)
                       ->setName('submit')
                       ->setLabel(trans('clearwebapps::form_widget.submit'))
                       )
            ;
        
        $this->setForm($form);
        
        
        parent::init();
        
        $this->getValidator()->setRules(array());
        
        return $this;
    }
}