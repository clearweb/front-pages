<?php namespace Clearweb\FrontPages\Gallery;

use Clearweb\FrontPages\Content\Image;
use Clearweb\Clearworks\Widget\ContainerWidget;

class GalleryWidget extends ContainerWidget
{
    private $images = array();
    
    public function init()
    {
        $this->setName('gallery-widget');
        
        $this->addViewable(with(new Gallery)->setImages($this->getImages()));
        
        parent::init();
        return $this;
    }
    
    public function getImages()
    {
        return $this->images;
    }
    
    public function setImages(array $images)
    {
        $this->images = $images;
        
        return $this;
    }
    
    public function addImage(Image $image) {
        $this->images[] = $image;
        
        return $this;
    }
}