<?php namespace Clearweb\FrontPages\Logo\Setting;

use Clearweb\Clearwebapps\Setting\SettingPackage;
use Clearweb\FrontPages\Logo\Widget\LogoSettingWidget;

class Package extends SettingPackage
{
    public function __construct()
    {
        $this->setName('logo');
        $this->setTitle(trans('front-pages::logo.logo_settings'));
        $this->addWidget(new LogoSettingWidget);
    }
}