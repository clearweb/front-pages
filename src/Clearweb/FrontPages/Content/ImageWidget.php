<?php namespace Clearweb\FrontPages\Content;

use Clearweb\Clearworks\Widget\ContainerWidget;

class ImageWidget extends ContainerWidget
{
    private $image = null;
    
    public function init()
    {
        parent::init();
        
        $this->getContainer()
            ->addViewable(
                          with(new Image)
                          ->setFile($this->getImage())
                          )
            ;
            
        return $this;    
    }
    
    public function getImage()
    {
        return $this->image;
    }
    
    public function setImage($image)
    {
        $this->image = $image;
        
        return $this;
    }
}