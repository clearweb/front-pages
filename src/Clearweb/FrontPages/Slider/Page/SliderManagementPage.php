<?php namespace Clearweb\FrontPages\Slider\Page;

use Clearweb\Clearwebapps\Page\Page;

use Clearweb\Clearwebapps\Layout\OverviewLayout;

use Clearweb\FrontPages\Slider\Widget\ListWidget;
use Clearweb\FrontPages\Slider\Widget\FormWidget;

use Clearworks;
use Menu;

class SliderManagementPage extends Page
{
    public function __construct()
    {
        $this->setSlug('slides');
    }
    
    public function init()
    {
        $this->setTitle(ucfirst(trans_choice('app.slide', 2)));
        
        /*
        $url = Clearworks::getPageUrl($this);
        Menu::addLink(ucfirst(trans_choice('app.slide', 1)), $url);
        */
        
        $this->setLayout(new OverviewLayout);
        
        $this->addWidgetLinear(new ListWidget, 'left')
            ->addWidgetLinear(new FormWidget, 'right')
            ;
        
        return parent::init();
    }
    
}