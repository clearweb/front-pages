<?php namespace Clearweb\FrontPages\Slider\Widget;

use Clearweb\Clearwebapps\Form\ImageUploadField;

class FormWidget extends \Clearweb\Clearwebapps\Eloquent\FormWidget
{
    /*
    function getModelClass()
    {
        return '\Clearweb\FrontPages\Slider\Slide';
    }
    */
    
    function init()
    {
        $this->setModelClass('\Clearweb\FrontPages\Slider\Slide');
        parent::init();
        
        $this->getForm()
            ->replaceField(
                          'image',
                          with(new ImageUploadField)
                          ->setName('image')
                          )
            ;
        
        return $this;
    }
}