$(document).ready(function() {
    var mySwiper = $('.swiper-container').swiper({
	mode:'horizontal',
	loop: true,
	calculateHeight: true,
	resizeReInit: true,
	grabCursor: true,
	speed: 1200,
	autoplay: 5000
    });
});